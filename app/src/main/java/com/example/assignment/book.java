package com.example.assignment;

public class book {
    String Book_name;
    String List_date;
    String Writer_name;
    String Language;


    public book() {

    }


    public String getBook_name() {
        return Book_name;
    }
    public void setBook_name(String book_name){
        Book_name = book_name;
    }
    public String getList_date() {
        return List_date;
    }
    public void setList_date(String list_date){
        List_date = list_date;
    }
    public String getWriter_name() {
        return Writer_name;
    }
    public void setWriter_name(String writer_name){
        Writer_name = writer_name;
    }

    public String getLanguage() {
        return Language;
    }
    public void setLanguage(String language){
        Language = language;
    }
}