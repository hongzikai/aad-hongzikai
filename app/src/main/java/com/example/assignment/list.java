package com.example.assignment;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class list extends AppCompatActivity {
    public EditText editTextName;
    public EditText editTextBorrower_name;
    public EditText editTextList_view;
    public EditText editTextReturn_view;
    public EditText editTextPhone;

    Button save;
    Button exit;
    Button view;
    Button delete;
    borrow borrow;
    DatabaseReference reff;
    long maxid = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        editTextName = (EditText) findViewById(R.id.nameText);
        editTextPhone = (EditText) findViewById(R.id.phone);
        editTextBorrower_name = (EditText) findViewById(R.id.borrower_name_Text);
        editTextList_view = (EditText) findViewById(R.id.list_view);
        editTextReturn_view = (EditText) findViewById(R.id.return_view);
        borrow = new borrow();
        reff = FirebaseDatabase.getInstance().getReference().child("borrow");
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    maxid=(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        save = (Button) findViewById(R.id.save);
        exit = (Button) findViewById(R.id.exit);
        view = (Button) findViewById(R.id.view);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(list.this, main_interface.class);
                startActivity(intent);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String borrower_name = editTextBorrower_name.getText().toString().trim();
                String list_view = editTextList_view.getText().toString().trim();
                String return_view = editTextReturn_view.getText().toString().trim();
                String phone = editTextPhone.getText().toString().trim();
                borrow.setBorrower_name(borrower_name);
                borrow.setList_view(list_view);
                borrow.setName(name);
                borrow.setReturn_view(return_view);
                borrow.setPhone(phone);
                reff.child(String.valueOf(maxid+1)).setValue(borrow);
                Toast.makeText(list.this,"data insert successfully",Toast.LENGTH_LONG).show();
            }

        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(list.this, borrow_list.class);
                startActivity(intent);
            }
        });}




}
