package com.example.assignment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class borrow_list extends AppCompatActivity {
    TextView Phone,book_name,Borrower_name,date,Return_view;
    Button show,exit;
    DatabaseReference reff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrow_list);

        Phone=(TextView)findViewById(R.id.p);
        book_name=(TextView)findViewById(R.id.b);
        Borrower_name=(TextView)findViewById(R.id.tb);
        date=(TextView)findViewById(R.id.d);
        Return_view=(TextView)findViewById(R.id.r);
        show = (Button) findViewById(R.id.show);
        exit = (Button) findViewById(R.id.exit);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(borrow_list.this, list.class);
                startActivity(intent);
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reff = FirebaseDatabase.getInstance().getReference().child("borrow").child("1");
                reff.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String name =dataSnapshot.child("name").getValue().toString();
                        String list_view =dataSnapshot.child("list_view").getValue().toString();
                        String return_view =dataSnapshot.child("return_view").getValue().toString();
                        String borrower_name =dataSnapshot.child("borrow_name").getValue().toString();
                        String phone =dataSnapshot.child("phone").getValue().toString();

                        Phone.setText(phone);
                        book_name.setText(name);
                        Borrower_name.setText(borrower_name);
                        date.setText(list_view);
                        Return_view.setText(return_view);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}