package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class make_qrcode extends AppCompatActivity implements View.OnClickListener{
    Button exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_qrcode);

        exit= (Button)findViewById(R.id.exit);
        Button button=(Button)findViewById(R.id.button2);
        button.setOnClickListener(this);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(make_qrcode.this, main_interface.class);
                startActivity(intent);
            }
        });
    }

    public void getCode(){
        ImageView ivCode=(ImageView)findViewById(R.id.imageView2);
        EditText etContent = (EditText)findViewById(R.id.editText);
        BarcodeEncoder encoder = new BarcodeEncoder();
        try{
            Bitmap bit = encoder.encodeBitmap(etContent.getText()
                    .toString(),BarcodeFormat.QR_CODE,250,250);
            ivCode.setImageBitmap(bit);
        }catch (WriterException e){
            e.printStackTrace();


        }
    }

    @Override
    public void onClick(View v) {
        getCode();

    }

}
