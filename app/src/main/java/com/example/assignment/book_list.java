package com.example.assignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class book_list extends AppCompatActivity {
    TextView Book_name,List_date,Writer_name,Language;
    Button show,exit;
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        Book_name=(TextView)findViewById(R.id.booktext);
        List_date=(TextView)findViewById(R.id.ld);
        Writer_name=(TextView)findViewById(R.id.wn);
        Language=(TextView)findViewById(R.id.la);
        show = (Button) findViewById(R.id.show);
        exit = (Button) findViewById(R.id.exit);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(book_list.this, system.class);
                startActivity(intent);
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref = FirebaseDatabase.getInstance().getReference().child("book").child("1");
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String book_name =dataSnapshot.child("book_name").getValue().toString();
                        String list_date =dataSnapshot.child("list_date").getValue().toString();
                        String writer_name =dataSnapshot.child("writer_name").getValue().toString();
                        String language =dataSnapshot.child("language").getValue().toString();


                        Book_name.setText(book_name);
                        List_date.setText(list_date);
                        Writer_name.setText(writer_name);
                        Language.setText(language);


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}
