package com.example.assignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class system extends AppCompatActivity {
    public EditText editTextBook_name;
    public EditText editTextWriter_name;
    public EditText editTextList_date;
    public EditText editTextLanguage;
    DatabaseReference ref;
    book book;
    long max_id = 0;

    Button save,exit,view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system);


        editTextBook_name = (EditText) findViewById(R.id.nameText);
        editTextWriter_name = (EditText) findViewById(R.id.writernameText);
        editTextLanguage = (EditText) findViewById(R.id.language_view);
        editTextList_date = (EditText) findViewById(R.id.list_view);
        book = new book();
        ref = FirebaseDatabase.getInstance().getReference().child("book");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    max_id=(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        view = (Button) findViewById(R.id.view);
        save = (Button) findViewById(R.id.save);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(system.this, main_interface.class);
                startActivity(intent);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextBook_name.getText().toString().trim();
                String language = editTextLanguage.getText().toString().trim();
                String list_date = editTextList_date.getText().toString().trim();
                String writer_name = editTextWriter_name.getText().toString().trim();
                book.setBook_name(name);
                book.setList_date(list_date);
                book.setLanguage(language);
                book.setWriter_name(writer_name);
                ref.child(String.valueOf(max_id+1)).setValue(book);
                Toast.makeText(system.this,"data insert successfully",Toast.LENGTH_LONG).show();
            }

        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(system.this, book_list.class);
                startActivity(intent);
            }
        });}
    }



